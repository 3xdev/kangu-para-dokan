<?php
// ADDING 2 NEW COLUMNS WITH THEIR TITLES (keeping "Total" and "Actions" columns at the end)
add_filter( 'manage_edit-shop_order_columns', '_3x_kangu_dokan_custom_shop_order_column', 20 );
function _3x_kangu_dokan_custom_shop_order_column($columns)
{
    $reordered_columns = array();
    // Inserting columns to a specific location
    foreach( $columns as $key => $column){
        $reordered_columns[$key] = $column;

        if( $key ==  'order_status' ){
            // Inserting after "Status" column
            $reordered_columns['kangu-dokan-status'] = __( 'Status do envio');
        }
    }
    return $reordered_columns;
}
function add_sort_register_sortable($cols) {
    $cols['kangu-dokan-status'] = 'statuses';
    return $cols;
}
add_filter("manage_edit-st_kb_sortable_columns", "add_sort_register_sortable");
// Adding custom fields meta data for each new column (example)
add_action( 'manage_shop_order_posts_custom_column' , '_3x_kangu_dokan_column_content', 20, 2 );
function _3x_kangu_dokan_column_content( $column, $post_id )
{
     if ($column ==  'kangu-dokan-status') {
            // Get custom post meta data
            $meta_rastreio = get_post_meta( $post_id, '_3x_kangu_status_rastreio', true ) ? get_post_meta( $post_id, '_3x_kangu_status_rastreio', true ) : '';
           echo $meta_rastreio;
    }
}
// function wpse331647_custom_query_vars_filter( $vars ) {
//     $vars[] .= '_3x_kangu_status_rastreio';
//     return $vars;
// }

// add_filter( 'query_vars', 'wpse331647_custom_query_vars_filter' );

// permite o filtro e pesquisa por status de postagem

function _3x_kangu_dokan_extend_admin_search( $query ) {
    $post_type = 'shop_order';
    $custom_fields = array("_3x_kangu_status_rastreio");

    if( ! is_admin() )
        return;
    if ( $query->query['post_type'] != $post_type )
        return;
    
    $orderby = $query->get('orderby');

    if ($orderby === '_3x_kangu_status_rastreio'){
        $query->set('meta_key','_3x_kangu_status_rastreio');
        $query->set('orderby','meta_value');
    }

    $search_term = $query->query_vars['s'];

    $query->query_vars['s'] = '';

    if ( $search_term != '' ) {
        $meta_query = array( 'relation' => 'OR' );
        foreach( $custom_fields as $custom_field ) {
            array_push( $meta_query, array(
                'key' => $custom_field,
                'value' => $search_term,
                'compare' => 'LIKE'
            ));
        }
        $query->set( 'meta_query', $meta_query );
    };
}
add_action( 'pre_get_posts', '_3x_kangu_dokan_extend_admin_search' );