<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function kangu_para_dokan_shipping_method_init() {
	class WC_Kangu_Dokan_Method extends WC_Shipping_Method {
		/**
		 * Constructor for your shipping class
		 *
		 * @access public
		 * @return void
		 */
		public function __construct($instance_id = 0) {
    		parent::__construct($instance_id);
			$this->id                 = 'kangu_para_dokan'; // Id for your shipping method. Should be uunique.
			$this->instance_id 		  = absint( $instance_id );
			$this->method_title       = __( 'Calculo de frete com Kangu para Dokan' );  // Title shown in admin
			$this->method_description = __( 'Description of your shipping method' ); // Description shown in admin
			$this->supports           = array(
				'shipping-zones',
				'instance-settings',
				'instance-settings-modal',
			);
			$this->instance_form_fields = array(
				'title' => array(
					'title' 		=> __( 'Nome do método de entrega' ),
					'type' 			=> 'text',
					'description' 	=> __( 'Esta opção controla o que o usuário vê no processo de checkout' ),
					'default'		=> __( 'Kangu' ),
					'desc_tip'		=> true
				),
				'token_kangu' => array(
					'title' 		=> __( 'Token de integração com a Kangu' ),
					'type' 			=> 'text',
					'description' 	=> __( 'Este é o token de api de integração usado para o cálculo de fretes' ),
					'default'		=> __( '' ),
					'desc_tip'		=> true
				)
			);
			$this->enabled              = 'yes';
			$this->title                = $this->get_option( 'title' );
			// Availability & Countries
			$this->availability = 'including';
			$this->countries = array('BR');
			$this->init();
		}

		/**
		 * Init your settings
		 *
		 * @access public
		 * @return void
		 */
		function init() {
			// Load the settings API
			$this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
			$this->init_settings(); // This is part of the settings API. Loads settings you previously init.

			// Save settings in admin if you have any defined
			add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
		}

		/**
		 * calculate_shipping function.
		 *
		 * @access public
		 * @param mixed $package
		 * @return void
		 */
		public function calculate_shipping($package = array()) {
				// error_log('Pacote do WC : '.json_encode($package));
				// $store_info  = dokan_get_store_info( $post_author_id );
				// $cep_origem = $store_info['address']['zip'];
				// pega o token do vendedor para calcular o frete pelo token dele
				$package_user_id = $package['seller_id'];
				// error_log('ID DO USUARIO NO PACOTE INSTANCIADO : '.json_encode($package_user_id));
				$token_vendedor = get_user_meta( $package_user_id, '_3x_dokan_kangu_key', true );
				
				if(!$token_vendedor){
					$token_vendedor = $this->get_option( 'token_kangu' );
				}
				
                foreach ($package['contents'] as $values) {
                    $_product = $values['data'];


                    $package['produtos'][] = array(
                        'peso'          => $_product->get_weight(),
                        'altura'        => $_product->get_height(),
                        'largura'       => $_product->get_width(),
                        'comprimento'   => $_product->get_length(),
                        'produto'       => $_product->get_name(),
                        'valor'         => $_product->get_price(),
                        'quantidade'    => $values['quantity']
                    );

                }
                // error_log('token enviado : '.json_encode($token_vendedor));
                $result = wp_remote_post('https://portal.kangu.com.br/tms/transporte/woocommerce-simular', [
                    'body'      => json_encode($package),
                    'timeout'   => 120,
                    'headers'   => [
                        'Content-Type' => 'application/json; charset=utf-8',
                        'token' => $token_vendedor
                    ]
                ]);

                error_log('pacote enviado na simulacao : '.json_encode($package) . 'fim do pacote');

                // error_log('resultado da simulacao : '.json_encode($result));

                wc_clear_notices();

                if ($result instanceof WP_Error) {
                    wc_add_notice($result->get_error_message(), 'error');
                } else {
                    if ($result['response']['code'] === 200) {
                        $fretes = json_decode($result['body'], true);
    
                        if ($fretes && is_array($fretes)) {
                            foreach ($fretes as $frete) {
                                $this->add_rate($frete);

                                if (isset($frete['alertas'])) {
                                    foreach ($frete['alertas'] as $alerta) {
                                        wc_add_notice($alerta);
                                    }
                                }

                                if (isset($frete['error']) && $frete['error']['message']) {
                                    foreach ($frete['error'] as $alerta) {
                                        wc_add_notice($frete['error']['message'], 'error');
                                    }
                                }
                            }
                        }
                    }
                }
            }
	}
}

add_action( 'woocommerce_shipping_init', 'kangu_para_dokan_shipping_method_init' );

function _3x_kangu_para_dokan_shipping_method( $methods ) {

	$methods['kangu_para_dokan'] =  'WC_Kangu_Dokan_Method';

	return $methods;
}

add_filter( 'woocommerce_shipping_methods', '_3x_kangu_para_dokan_shipping_method' );