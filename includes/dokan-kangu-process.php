<?php
/**
 * summary
 */
class _3X_DOKAN_PROCESS {
    /**
     * summary
     */
    public function __construct(){
        
    }

    private function curly($operation, $token, $data){

    	$json = json_encode($data);
		$curlHandler = curl_init();

		curl_setopt_array($curlHandler, [
			// CURLOPT_URL => 'https://portal.kangu.com.br/tms/transporte/simular',
			CURLOPT_URL => 'https://portal.kangu.com.br/tms/transporte/'.$operation,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $json,
			CURLOPT_HTTPHEADER => array(
				'Content-Type:application/x-www-form-urlencoded',
				// 'token: 26ebcb5b92e7f7f20aa25d0a97535470',
				'token:' . $token,
				'Content-Length: ' . strlen($json)
				)
		]);
		$response = curl_exec($curlHandler);
		curl_close($curlHandler);
		
		error_log('resposta: '. $response);

		return $response;
    }

    // essa função serve para retornar os valores de fretes para a página do produto dinâmicamente
    public function calcular_frete_ajax($dados_do_produto, $cep_origem, $cep_destino, $token){
    	$operation = 'woocommerce-simular';
		$data = array();
		$data['contents']['quantity'] = $dados_do_produto['quantidade'];
		
		$data['destination']['postcode'] = $cep_destino;
		

		$quantidade_loop = $dados_do_produto['quantidade'];

		// for ($i = 0; $i <= $quantidade_loop; $i++) {
		$data['produtos'][] = 
			[
				'peso'=>$dados_do_produto['peso'],
				'altura'=> $dados_do_produto['altura'],
				'largura'=> $dados_do_produto['largura'],
				'comprimento'=> $dados_do_produto['comprimento'],
				'tipo' => 'C',
				'valor' => $dados_do_produto['preco'],
				'quantidade' =>  $dados_do_produto['quantidade']
			];

		error_log('dados informados: ' . json_encode($data));

		$result = wp_remote_post('https://portal.kangu.com.br/tms/transporte/woocommerce-simular', [
            'body'      =>json_encode($data),
            'timeout'   => 120,
            'headers'   => [
                'Content-Type' => 'application/json; charset=utf-8',
                'token' => $token
            ]
        ]);

		return $result;
		
    }

    public function importar_pedido($store_info, $order, $parent_order, $seller_kangu_info){
    	// essa não precisa explicar
    	$operation = 'solicitar';
    	// instancio o pedido pai para pegar alguns dados que o dokan não copia para os subpedidos.
    	if($parent_order == 0){
    		$parent_order_meta = new WC_Order( $order );
    	}else{
    		$parent_order_meta = new WC_Order( $parent_order );
    	}
    	// cada pedido é importado com o token de cada seller obviamente
    	$token = $seller_kangu_info['token'];
    	// variaveis auxiliares
    	$volumes = array();
		$total_weight = 0;
		// pega o nr do id da transportadora do woocommerce
		// estou retornando a chave do array pq o dokan é foda e tem o propria forma de armazenar os dados de entrega
		// deve haver uma forma melhor de fazer isso
    	$shipping_methods = $order->get_shipping_methods();
    	$shipping_method = preg_replace('/[^A-Za-z0-9\-]/', '', json_encode(array_keys($shipping_methods) ) );
    	// pega o valor dos itens sem taxas e sem frete
    	$valor_mercadorias = number_format( (float) $order->get_total() - $order->get_total_tax() - $order->get_total_shipping() - $order->get_shipping_tax(), wc_get_price_decimals(), '.', '' );

    	// itera pelo pedido retornando cada item
    	foreach ( $order->get_items() as $item_id => $item) {
		    
		   $product = $item->get_product();

		   $total_weight += get_post_meta( $item['product_id'], '_weight', true );

		   $shipping_data = [
				'peso' => get_post_meta( $item['product_id'], '_weight', true ),
				'altura' => get_post_meta( $item['product_id'], '_height', true ),
				'largura' => get_post_meta( $item['product_id'], '_width', true ),
				'comprimento' => get_post_meta($item['product_id'], '_length', true ),
				'tipo' => 'C',
				'produto' => $item->get_name(),
				'valor' => sprintf("%0.2f",$product->get_price_including_tax()),
				'quantidade' => $item->get_quantity(),
			];

		    $volumes[] = $shipping_data;
		}

    	$data = [
			'pedido' => [
				'tipo' => 'D',
				'numeroCli' => $order->get_id(),
				'vlrMerc' => $valor_mercadorias,
				'pesoMerc' => $total_weight,
			],
			'origem' => 'cshof',
			'remetente' => [
				'nome' => $store_info['store_name'],
				'cnpjCpf' => $seller_kangu_info['documento'],
					'endereco' => [
						'logradouro' => $store_info['address']['street_1'],
						'numero' => $seller_kangu_info['numero'],
						'complemento' => $store_info['address']['street_2'],
						'bairro' => $seller_kangu_info['bairro'],
						'cep' => $store_info['address']['zip'],
						'cidade' => $store_info['address']['city'],
						'uf' => $store_info['address']['state'],
					],
				'contato' => $store_info['store_name'],
				'email' => 'contato@casahomeoffice.com',
				'telefone' =>  $store_info['phone'],
				'celular' => $store_info['phone']
			],
			'destinatario' => [
				'nome' => $parent_order_meta->get_formatted_billing_full_name(),
				'cnpjCpf' =>  ($parent_order_meta->get_meta('_billing_cpf'))?$order->get_meta('_billing_cpf'):$order->get_meta('_billing_cnpj'),
				'endereco' => [
					'logradouro' => $parent_order_meta->get_billing_address_1(),
					'numero' => $parent_order_meta->get_meta('_billing_number'),
					'complemento' => $parent_order_meta->get_billing_address_2(),
					'bairro' => $parent_order_meta->get_meta('_billing_neighborhood'),
					'cep' => $parent_order_meta->get_billing_postcode(),
					'cidade' => $parent_order_meta->get_billing_city(),
					'uf' => $parent_order_meta->get_billing_state(),
				],
				'contato' => $parent_order_meta->get_billing_first_name(),
				'email' => $parent_order_meta->get_billing_email(),
				'telefone' => $parent_order_meta->get_billing_phone(),
				'celular' => $parent_order_meta->get_meta('_billing_cellphone'),
			],
			'volumes' => $volumes,
			'servicos' => ['P','R'],
			'referencia' => wc_get_order_item_meta($shipping_method, 'referencia_kangu', true)
		];

		error_log('data :'.json_encode($data));

    	$resultado = self::curly($operation, $token, $data);

    	$resultado = json_decode($resultado);

    	if($resultado->codigo){
    		update_post_meta( $order->get_id(), '_3x_kangu_campo_rastreio', $resultado->codigo);
    		update_post_meta( $order->get_id(), '_3x_kangu_etiqueta_envio', $resultado->etiquetas);
    		update_post_meta( $order->get_id(), '_3x_kangu_data_prevista', $resultado->dtPrevEnt);
    		update_post_meta( $order->get_id(), '_3x_kangu_tarifas_envio', $resultado->tarifas);
    	}

    }
    private function token_pelo_nr_do_pedido($pedido){
    	$post_author_id = get_post_field( 'post_author', $pedido );
    	$token = get_user_meta( $post_author_id, '_3x_dokan_kangu_key', true );

    	return $token;
    }

    public function imprimir_etiqueta($pedido, $etiqueta){
    	$json = '{"codigo": '.$etiqueta.'"}';
    	$operation = 'imprimir-etiqueta/'.$etiqueta;
    	$token = self::token_pelo_nr_do_pedido($pedido);

    	$result = self::curly($operation, $token, $json);

    	if ( isset($result['pdf']) && $result['pdf'] ){
			$pdf = base64_decode( json_decode($result['pdf'], true) );
			return $pdf;
		} else {
			$error = json_decode($result, true);
			$cod_err = $error['error']['codigo'];
			$msg_err = $error['error']['mensagem'];

			return $msg_err;
		}
    }

    public function rastrear_pedido($pedido, $cod_rastreio){
    	$data = json_encode('');
    	$operation = 'rastrear/'.$cod_rastreio;
    	$token = self::token_pelo_nr_do_pedido($pedido);

    	return self::curly($operation, $token, $data);
    }
}