<?php 
function _3x_dokan_kangu_custom_cron_schedule( $schedules ) {
    $schedules['every_six_hours'] = array(
        'interval' => 21600, // Every 6 hours
        'display'  => __( 'Every 6 hours' ),
    );
    return $schedules;
}
add_filter( 'cron_schedules', '_3x_dokan_kangu_custom_cron_schedule' );

//create your function, that runs on cron
function _3x_dokan_kangu_cron_function() {
    //your function...
    // Get orders processing and on-hold.
	$args = array(
	    'status' => array('wc-processing', 'wc-on-hold', 'wc-pending'),
	);
	$orders = wc_get_orders( $args );

	foreach ($orders as $order){

		$order_id = $order->get_id();

		$meta_rastreio = get_post_meta( $order_id, '_3x_kangu_campo_rastreio', true ) ? get_post_meta( $order_id, '_3x_kangu_campo_rastreio', true ) : '';
    	$rastreio =   (new _3X_DOKAN_PROCESS)->rastrear_pedido($order_id, str_replace(' ', '', $meta_rastreio));

    	$rastreio = json_decode($rastreio, true);

    	if(!isset($rastreio['situacao']) || $rastreio['situacao'] == ""){
    		update_post_meta($order_id, '_3x_kangu_status_rastreio', $rastreio['situacao']);	
    	}else{
    		update_post_meta($order_id, '_3x_kangu_status_rastreio', $rastreio['error']['mensagem']);	
    	}
    	
	}

	// error_log('croning :'.json_encode($orders));

}
///Hook into that action that'll fire every six hours
add_action( '_3x_dokan_kangu_cron_hook', '_3x_dokan_kangu_cron_function' );

//Schedule an action if it's not already scheduled
if ( ! wp_next_scheduled( '_3x_dokan_kangu_cron_hook' ) ) {
    wp_schedule_event( time(), 'every_six_hours', '_3x_dokan_kangu_cron_hook' );
}

register_deactivation_hook( __FILE__, '_3x_dokan_kangu_deactivating' ); 
 
function _3x_dokan_kangu_deactivating() {

    $timestamp = wp_next_scheduled( '_3x_dokan_kangu_cron_hook' );
    wp_unschedule_event( $timestamp, '_3x_dokan_kangu_cron_hook' );

}
