<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function dokan_kangu_atualiza_config_token(){

	if(!isset($_POST)){
		wp_die();
	}
    
    if(!isset($_POST['token_kangu']) || $_POST['token_kangu'] == ''){
    	echo 'Dados imcompletos, por favor preencha todos os campos corretamente.';
    	wp_die();
    }
    if(!isset($_POST['documento'])  || $_POST['documento'] == '' ){
    	echo 'Dados imcompletos, por favor preencha todos os campos corretamente.';
    	wp_die();
    }
    if( !isset($_POST['bairro'])  || $_POST['bairro'] == '' ){
    	echo 'Dados imcompletos, por favor preencha todos os campos corretamente.';
    	wp_die();
    }
    if(!isset($_POST['numero'])  || $_POST['numero'] == '' ){
    	echo 'Dados imcompletos, por favor preencha todos os campos corretamente.';
    	wp_die();
    }

	$token_kangu =  sanitize_text_field( $_POST['token_kangu'] );
	$documento_kangu =  sanitize_text_field( $_POST['documento'] );
	$bairro_kangu =  sanitize_text_field( $_POST['bairro'] );
	$numero_kangu =  sanitize_text_field( $_POST['numero'] );

	// $current_zip = get_user_meta($currentUser->ID, 'dokan_profile_settings', true);
	$currentUser = wp_get_current_user();

 	$updateToken = update_user_meta($currentUser->ID, '_3x_dokan_kangu_key', $token_kangu);
 	$updateDoc = update_user_meta($currentUser->ID, '_3x_dokan_kangu_cpfcnpj', $documento_kangu);
 	$updateBairro = update_user_meta($currentUser->ID, '_3x_dokan_kangu_bairro', $bairro_kangu);
 	$updateNR = update_user_meta($currentUser->ID, '_3x_dokan_kangu_nr', $numero_kangu);

	echo 'Dados atualizados'; 		
 	
	// echo $token_kangu;
	

	wp_die(); // this is required to terminate immediately and return a proper response
        
}

add_action('wp_ajax_dokan_kangu_config_token', 'dokan_kangu_atualiza_config_token');
add_action('wp_ajax_nopriv_dokan_kangu_config_token', 'dokan_kangu_atualiza_config_token');



function dokan_kangu_calcula_frete_ajax(){

	if(!isset($_POST) || $_POST['cep'] == ''){
		echo '{"error": "cep invalido"}';
		wp_die();
	}

	// dados recebidos do formulário
	$produto = sanitize_text_field($_POST['produto']);
	$cep_destino = sanitize_text_field($_POST['cep']);
	$quantidade = sanitize_text_field($_POST['quantidade']);

    $dados_do_produto = array();
	// Instancia o produto pelo id
	$product = wc_get_product( $produto );
    $dimensions = $product->get_dimensions( false );

    // pega o peso e medida do produto e salva em um array
	if ( ! empty( $dimensions ) ) {
		$dados_do_produto['altura'] = $product->get_height();
		$dados_do_produto['largura'] = $product->get_width();
		$dados_do_produto['comprimento'] = $product->get_length();
		$dados_do_produto['peso'] = $product->get_weight();
		$dados_do_produto['preco'] = $product->get_price();
		$dados_do_produto['quantidade'] = $quantidade;
	}
	// pega o id do vendedor para pegar seus dados
	$post_author_id = get_post_field( 'post_author', $produto );
	// pega o token do vendedor para calcular o frete pelo token dele	
	$token_vendedor = get_user_meta( $post_author_id, '_3x_dokan_kangu_key', true );
	// instancia os dados da loja
	$store_info  = dokan_get_store_info( $post_author_id );
	$cep_origem = $store_info['address']['zip'];

	if(!$cep_origem || $cep_origem === ''){
		$cep_origem = get_option( 'woocommerce_store_postcode' );
	}

	$resultado_calculo = (new _3X_DOKAN_PROCESS)->calcular_frete_ajax($dados_do_produto, $cep_origem, $cep_destino, $token_vendedor);

	if ($resultado_calculo['response']['code'] === 200) {
        $fretes = json_decode($resultado_calculo['body'], true);
    }
	
	echo json_encode($fretes);

	wp_die(); // this is required to terminate immediately and return a proper response
        
}

add_action('wp_ajax_dokan_kangu_calcula_frete_ajax', 'dokan_kangu_calcula_frete_ajax');
add_action('wp_ajax_nopriv_dokan_kangu_calcula_frete_ajax', 'dokan_kangu_calcula_frete_ajax');

function dokan_kangu_imprime_etiqueta(){
	if(!isset($_POST)){
		wp_die();
	}
	$pedido = sanitize_text_field($_POST['pedido']);
	$etiqueta = sanitize_text_field($_POST['etiqueta']);

	$etiqueta_para_impressao = (new _3X_DOKAN_PROCESS)->imprimir_etiqueta($pedido, $etiqueta);

	echo $etiqueta_para_impressao;

	wp_die();
}
add_action('wp_ajax_dokan_kangu_imprime_etiqueta', 'dokan_kangu_imprime_etiqueta');
add_action('wp_ajax_nopriv_dokan_kangu_imprime_etiqueta', 'dokan_kangu_imprime_etiqueta');