<?php
defined( 'ABSPATH' ) || exit;

// executa a funcao sempre que o status de um pedido for alterado 
add_action('woocommerce_order_status_changed','dokan_kangu_status_change_custom', 10 , 3);

// Esta função é disparada quando o status do pedido é alterado para "processando"
// O status processando é o equivalente à pedido pago.
// uma vez pago, envia os dados do pedido para a kangu, salva os dados de rastreio e libera a impressão de etiqueta
function dokan_kangu_status_change_custom($order_id, $old_status, $new_status) {

    if($new_status === "processing"){

        $order = new WC_Order( $order_id );
        $sellers = dokan_get_seller_id_by_order( $order );
        
        $parent_order = $order->parent_id;

        error_log("order: ".$order);
        error_log("sellers: ".json_encode($sellers));

        // verifica se o pedido tem sub-pedidos
        if ( $order->get_meta('has_sub_order') ) {
                return;

        } else {
            // pega os dados do único vendedor
            $seller_info = get_userdata( $sellers );

            $store_info  = dokan_get_store_info( $seller_info->ID );

            $seller_kangu_info['token'] = get_user_meta( $seller_info->ID, '_3x_dokan_kangu_key', true );
            $seller_kangu_info['documento'] = get_user_meta( $seller_info->ID, '_3x_dokan_kangu_cpfcnpj', true );
            $seller_kangu_info['bairro'] = get_user_meta( $seller_info->ID, '_3x_dokan_kangu_bairro', true );
            $seller_kangu_info['numero'] = get_user_meta( $seller_info->ID, '_3x_dokan_kangu_nr', true );

            // error_log('seller_info: '.json_encode($store_info));
            // error_log('seller_info: '.json_encode($seller_info));

            (new _3X_DOKAN_PROCESS)->importar_pedido($store_info, $order, $parent_order, $seller_kangu_info);
        }
    }
}