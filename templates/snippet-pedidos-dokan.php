<?php
add_action('woocommerce_admin_order_item_headers', '_3x_snippet_de_pedidos_da_kangu', 10, 1);

function _3x_snippet_de_pedidos_da_kangu() {
    if(is_admin()) return;

    $order_id = isset( $_GET['order_id'] ) ? intval( $_GET['order_id'] ) : 0;
    // $order = new WC_Order($post->ID);
    $meta_rastreio = get_post_meta( $order_id, '_3x_kangu_campo_rastreio', true ) ? get_post_meta( $order_id, '_3x_kangu_campo_rastreio', true ) : '';
    $meta_etiquetas = get_post_meta( $order_id, '_3x_kangu_etiquetas_envio', true ) ? get_post_meta( $order_id, '_3x_kangu_etiquetas_envio', true ) : '';

    $etiqueta_envio = $meta_etiquetas[0]->numero;

    // $order_id = trim(str_replace('#', '', $order->get_order_number()));
    $rastreio = json_decode( (new _3X_DOKAN_PROCESS)->rastrear_pedido($order_id, str_replace(' ', '', $meta_rastreio)), true);

    if($rastreio){
        $rastreio = $rastreio['error'];
    }

?>
    <div class="dokan-panel dokan-panel-default">
        <div class="dokan-panel-heading"><strong>Rastreamento com Kangu <? echo $slug ?></strong> </div>
        <div class="dokan-panel-body" id="woocommerce-order-items">
            <input type="hidden" name="" id="meta-rastreio" value="<?php echo str_replace(' ', '', $meta_rastreio) ?>">
            <input type="hidden" name="" id="etiqueta-kangu" value="<?php echo $etiqueta_envio ?>">
            <!-- ... conteudo -->
            <div>
                <div><b>Código de rastreio: </b><?php echo $meta_rastreio ?></div>
                <div><b>Status do envio: </b><?php  echo $rastreio['mensagem']  ?></div>
            </div>
            <!-- <div class="buttons" style="display: flex;justify-content: space-around;">
                <button type="button" id="imprimir_etiqueta">Imprimir etiqueta de envio</button>
            </div> -->
            <a class="dokan-btn dokan-btn-theme dokan-add-new-product" id="imprimir_etiqueta">
            
            <i class="fa fa-print"></i>
                Imprimir etiqueta de envio                                      
            </a>
        </div>

    </div>
<?php 
}