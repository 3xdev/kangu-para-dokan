<?php
defined( 'ABSPATH' ) || exit;
/**
 * Plugin Name: Dokan Kangu
 * Plugin URI: https://3xweb.site
 * Description: Integra o meio de transporte kangu ao Dokan Marketplace
 * Author URI: https://3xweb.site/
 * Version: 1.0.0
 * Requires at least: 4.4
 * Tested up to: 5.7
 * WC requires at least: 3.0
 * WC tested up to: 5.4
 * Text Domain: dokan-kangu
 * Domain Path: /languages
 */

/**
 * Required minimums and constants
 */
add_filter( 'dokan_query_var_filter', 'kangu_dokan_load_document_menu' );
function kangu_dokan_load_document_menu( $query_vars ) {
    $query_vars['kangu'] = 'kangu';
    return $query_vars;
}
add_filter( 'dokan_get_dashboard_nav', 'kangu_dokan_add_help_menu' );
function kangu_dokan_add_help_menu( $urls ) {
    $urls['kangu'] = array(
        'title' => __( 'Kangu', 'dokan'),
        'icon'  => '<i class="fa fa-truck"></i>',
        'url'   => dokan_get_navigation_url( 'kangu' ),
        'pos'   => 51
    );
    return $urls;
}
add_action( 'dokan_load_custom_template', 'dokan_load_template' );
function dokan_load_template( $query_vars ) {
    if ( isset( $query_vars['kangu'] ) ) {
        require_once dirname( __FILE__ ). '/templates/kangu-config.php';
       }
}

function _3x_Dokan_Kangu() {

    static $plugin;

    if ( ! isset( $plugin ) ) {

        class _3X_DOKAN_KANGU {

            /**
             * The *Singleton* instance of this class
             *
             * @var Singleton
             */
            private static $instance;

            /**
             * Returns the *Singleton* instance of this class.
             *
             * @return Singleton The *Singleton* instance.
             */
            public static function get_instance() {
                if ( null === self::$instance ) {
                    self::$instance = new self();
                }
                return self::$instance;
            }

            /**
             * Private clone method to prevent cloning of the instance of the
             * *Singleton* instance.
             *
             * @return void
             */
            public function __clone() {}

            /**
             * Private unserialize method to prevent unserializing of the *Singleton*
             * instance.
             *
             * @return void
             */
            public function __wakeup() {}

            /**
             * Protected constructor to prevent creating a new instance of the
             * *Singleton* via the `new` operator from outside of this class.
             */
            public function __construct() {
                add_action( 'admin_init', [ $this, 'install' ] );

                if ( class_exists( 'WooCommerce' ) ) {
                    if ( is_admin() ) {
                        $this->admin_includes();
                    }

                    $this->includes();
                    // add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );
                } else {
                    echo 'error loading this';
                }
            }
            /**
             * Handles upgrade routines.
             *
             * @since 3.1.0
             * @version 3.1.0
             */
            public function install() {
                if ( ! is_plugin_active( plugin_basename( __FILE__ ) ) ) {
                    return;
                }

            }
            private function includes() {
                include_once dirname( __FILE__ ) . '/includes/dokan-kangu-order-status-updater.php';
                include_once dirname( __FILE__ ) . '/includes/dokan-kangu-process.php';
                include_once dirname( __FILE__ ) . '/includes/dokan-kangu-shipping-method.php';
                include_once dirname( __FILE__ ) . '/includes/dokan-kangu-custom-order-metabox.php';
                include_once dirname( __FILE__ ) . '/includes/dokan-kangu-spy-scheduler.php';
                
                include_once dirname( __FILE__ ) . '/templates/single-ajax-shipping.php';
                include_once dirname( __FILE__ ) . '/templates/snippet-pedidos-dokan.php';
                include_once dirname( __FILE__ ) . '/templates/dokan-dica-de-frete.php';
            }
            /**
             * Admin includes.
             */
            private function admin_includes() {
                
                include_once dirname( __FILE__ ) . '/includes/dokan-kangu-ajax.php';
                include_once dirname( __FILE__ ) . '/includes/dokan-kangu-order-spy.php';

            }

        }

        $plugin = _3X_DOKAN_KANGU::get_instance();

    }

    return $plugin;
}


add_action( 'plugins_loaded', '_dokan_kangu_servico_init' );

function _dokan_kangu_servico_init() {
    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

    load_plugin_textdomain( 'dokan-kangu', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );

    // verifica se os campos do Brazilian Market estão configurados de forma favorável

    // carregando scripts adicionais
    add_action( 'admin_enqueue_scripts', '_3xweb_dokan_kangu_scriptLoader' );

    function _3xweb_dokan_kangu_scriptLoader(){
        // Register each script
        wp_register_script(
            'dokan_kangu_javascript', 
            plugins_url('/resources/js/dokan-kangu-javascript.js', __FILE__ ), 
            array( 'jquery' ),
            false,
            true
        );
        wp_register_style('dokan_kangu_css', plugins_url('/resources/css/style.css',__FILE__ ));

        wp_enqueue_script('dokan_kangu_javascript');
        wp_enqueue_style('dokan_kangu_css');
        // wp_enqueue_style('dashicons');
    }
    /**
     * Carregando scripts publicos
     */
    function xweb_dokan_kangu_publicScriptLoader($hook) {
     
        // create my own version codes
        $my_js_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . '/resources/js/' ));
         
        // 
        wp_enqueue_script( 'xweb_dokan_kangu_public_js', plugins_url( '/resources/js/public-scripts.js', __FILE__ ), array(), $my_js_ver );
        wp_enqueue_script( 'xweb_dokan_kangu_mask', plugins_url( '/resources/js/jquery.mask.min.js', __FILE__ ), array(), $my_js_ver );

        wp_localize_script( 'xweb_dokan_kangu_public_js', 'kangu_ajax_object',
                array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
     
    }
    add_action('wp_enqueue_scripts', 'xweb_dokan_kangu_publicScriptLoader');

    _3x_Dokan_Kangu();
}