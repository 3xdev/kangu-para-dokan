<?php
defined( 'ABSPATH' ) || exit;
// Adding Meta container admin shop_order pages
add_action( 'add_meta_boxes', 'woo_os_order_add_meta_boxes' );
if ( ! function_exists( 'woo_os_order_add_meta_boxes' ) )
{
    function woo_os_order_add_meta_boxes()
    {
        add_meta_box( 'mv_other_fields', __('Rastreio Kangu','woocommerce'), '_3x_kangu_campo_rastreio', 'shop_order', 'side', 'core' );
    }
}

// Adding Meta field in the meta container admin shop_order pages
if ( ! function_exists( '_3x_kangu_campo_rastreio' ) )
{
    function _3x_kangu_campo_rastreio()
    {
        global $post;

        $meta_rastreio = get_post_meta( $post->ID, '_3x_kangu_campo_rastreio', true ) ? get_post_meta( $post->ID, '_3x_kangu_campo_rastreio', true ) : '';

        echo '<p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
            <input type="text" style="width:250px;" name="serial_do_pedido" placeholder="' . $meta_rastreio . '" value="' . $meta_rastreio . '" readonly="true"></p>';

            if(!empty($meta_field_data)){
                echo '
                <a href="https://www.kangu.com.br/rastreio/?codigo='.$meta_rastreio.'" target="_blank">
                <input class="button-secondary" type="button" name="ver_os" id="ver_os" value="Rastrear pedido" />
                </a>';
            }



    }
}