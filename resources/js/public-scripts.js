function x_dokan_kangu_atualiza_config_token(token_kangu, documento, bairro, numero){

    var data = {
        'action': 'dokan_kangu_config_token',
        'token_kangu': token_kangu,
        'documento': documento,
        'bairro': bairro,
        'numero': numero
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    jQuery.post(kangu_ajax_object.ajax_url, data, function(response) {
        if(response){
            // console.log(response);
            alert(response);
        }

    });    
}

function x_dokan_kangu_calcula_frete_ajax(id_produto, cep, quantidade){
    if(cep == '') return;

    var data = {
        'action': 'dokan_kangu_calcula_frete_ajax',
        'produto': id_produto,
        'cep': cep,
        'quantidade': quantidade
    };
    jQuery(".result, .error" ).remove();
    jQuery( ".wait" ).addClass( "myloader" );

    jQuery.post(kangu_ajax_object.ajax_url, data, function(response) {        
        if(response){
            console.log('resposta :'+response);
            
            jQuery( ".wait" ).removeClass( "myloader" );
            
            response = JSON.parse(response); 

            jQuery.each(response, function(i, item) {
                jQuery( ".shipping-results" ).append( 
                    '<div class="result"><div><i class="fas fa-truck"></i> '+
                    response[i].meta_data.shipping_label
                    +'</div><div><i class="fa fa-clock-o" aria-hidden="true"></i> '
                    +response[i].meta_data.deadline+
                    '</div><div>R$ '
                    +response[i].cost+
                    '</div></div>'
                );
            });
            // }
            
        }

    });    
}
function x_dokan_kangu_imprime_etiqueta_ajax(pedido, etiqueta){
    var data = {
        'action' : 'dokan_kangu_imprime_etiqueta',
        'pedido': pedido,
        'etiqueta' : etiqueta,
    }
    jQuery.post(kangu_ajax_object.ajax_url, data, function(response) {        
        if(response){
            alert(response);
        }

    });   
}

jQuery(function() {
    jQuery('body').on("click", "#cepsend", function(e){
        e.preventDefault();
            
            var cep = jQuery( "#cepvalue" ).val();
            var id_produto = jQuery( "#idproduto" ).val();
            var quantidade = jQuery('.qty').val();

         x_dokan_kangu_calcula_frete_ajax(id_produto, cep, quantidade);      
    });

    jQuery('body').on("click", "#imprimir_etiqueta", function(e){
        e.preventDefault();
            
            var pedido = jQuery( "#post-id" ).val();
            var etiqueta = jQuery( "#meta-rastreio" ).val();

         x_dokan_kangu_imprime_etiqueta_ajax(pedido, etiqueta);      
    });

    $('#cepvalue').mask('00000-000');
});