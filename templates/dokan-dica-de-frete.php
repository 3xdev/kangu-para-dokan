<?php

add_action( 'dokan_product_options_shipping', 'dica_de_frete_kangu', 20 );

function dica_de_frete_kangu(){
	?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
		    $("#_weight").attr({
		       "max" : 30,        // substitute your own
		       "min" : 0          // values (or variables) here
		    });
		    $("#_length").attr({
		       "max" : 105,        // substitute your own
		       "min" : 16          // values (or variables) here
		    });
		    $("#_width").attr({
		       "max" : 100,        // substitute your own
		       "min" : 11          // values (or variables) here
		    });
		    $("#_height").attr({
		       "max" : 100,        // substitute your own
		       "min" : 1          // values (or variables) here
		    });
		});
	</script>
	<div class="woocommerce-notices-wrapper">
		<div class="woocommerce-info">	
		 	<b>ATENÇÃO: RESTRIÇÕES DE ENVIO</b><br>
			<b>Peso:</b> O máximo permitido é de 30kg<br>
			<b>Altura:</b> Permitidos pacotes entre 1 a 100cm<br>
			<b>Largura:</b> Permitidos pacotes entre 11 a 100cm<br>
			<b>Comprimento:</b> Permitidos pacotes entre 16 a 105cm
		</div>
	</div>
	<?php
} 