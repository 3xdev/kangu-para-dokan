<?php
/**
 *  Dokan Dashboard Template
 *
 *  Dokan Main Dahsboard template for Fron-end
 *
 *  @since 2.4
 *
 *  @package dokan
 */
?>
<div class="dokan-dashboard-wrap">
    <?php
        /**
         *  dokan_dashboard_content_before hook
         *
         *  @hooked get_dashboard_side_navigation
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_before' );
    ?>

    <div class="dokan-dashboard-content">

        <?php
            /**
             *  dokan_dashboard_content_before hook
             *
             *  @hooked show_seller_dashboard_notice
             *
             *  @since 2.4
             */
            do_action( 'dokan_help_content_inside_before' );
        ?>
         <?php 
            $currentUser = wp_get_current_user();
            
            // print_r($current_zip['address']['zip']);
        ?>
        <article class="help-content-area">
        	<h1>Configure seu token kangu</h1>
          	<p>
                Para realização a integração de frete Kangu® é preciso informar sua chave de integração abaixo.

                A integração kangu permite:
                <ol>
                    <li>Cálculo de frete automático</li>
                    <li>Impressão de etiquetas kangu</li>
                    <li>Envio de pedidos via kangu</li>
                </ol>

                Cole aqui seu token kangu para habilitar estes recursos.
                <br>
                <b>Atenção: </b> Você deve informar os mesmos dados que utilizou em seu cadastro na kangu.
            </p>
            <form role="form" method="post">
            <div class="dokan-form-group">
                <label for="token-kangu">Seu token kangu</label>
                <input class="dokan-form-control" id="token-kangu" type="text" name="token-kangu" value="<?php echo get_user_meta($currentUser->ID, '_3x_dokan_kangu_key', true); ?>">
            </div>
            <div class="dokan-form-group">
                <label for="token-kangu">Seu CPF ou CNPJ</label>
                <input class="dokan-form-control" id="documento-kangu" type="text" name="documento-kangu" value="<?php echo get_user_meta($currentUser->ID, '_3x_dokan_kangu_cpfcnpj', true); ?>">
            </div>
            <div class="dokan-form-group">
                <label for="token-kangu">Bairro da sua loja</label>
                <input class="dokan-form-control" id="bairro-kangu" type="text" name="bairro-kangu" value="<?php echo get_user_meta($currentUser->ID, '_3x_dokan_kangu_bairro', true); ?>">
            </div>
            <div class="dokan-form-group">
                <label for="token-kangu">Número do endereço da sua loja</label>
                <input class="dokan-form-control" id="numero-kangu" type="text" name="numero-kangu" value="<?php echo get_user_meta($currentUser->ID, '_3x_dokan_kangu_nr', true); ?>">
            </div>
            <div class="dokan-form-group">
                <button type="submit" id="kangu_action" class="button button-small">Salvar</button>
            </div>
            </form>

            <div>
                 <p>
                   <b>Não tem um token?</b> <a href="https://portal.kangu.com.br/meu-acesso" target="_blank" class="dokan-btn dokan-btn-danger dokan-btn-theme">Obter meu token Kangu</a> 
                </p>
            </div>
            

        </article><!-- .dashboard-content-area -->
        <script type="text/javascript">
            jQuery(function() {
            // console.log( "pronto para executar o js!" );
            jQuery('body').on("click", "#kangu_action", function(e){
                e.preventDefault();
                    
                    var token_kangu = jQuery( "#token-kangu" ).val();
                    var documento = jQuery( "#documento-kangu" ).val();
                    var bairro = jQuery( "#bairro-kangu" ).val();
                    var numero = jQuery( "#numero-kangu" ).val();

                 x_dokan_kangu_atualiza_config_token(token_kangu, documento, bairro, numero);      
            });

        });
        </script>
         <?php
            /**
             *  dokan_dashboard_content_inside_after hook
             *
             *  @since 2.4
             */
            do_action( 'dokan_dashboard_content_inside_after' );
        ?>


    </div><!-- .dokan-dashboard-content -->

    <?php
        /**
         *  dokan_dashboard_content_after hook
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_after' );
    ?>

</div><!-- .dokan-dashboard-wrap -->