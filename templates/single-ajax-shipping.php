<?php
add_action( 'woocommerce_after_add_to_cart_button', 'add_content_after_addtocart_button_func' );
/*
 * Content below "Add to cart" Button.
 */
function add_content_after_addtocart_button_func() {
        global $product;
        $id = $product->get_id();
        // Echo content.
        ?>
        <style type="text/css">
                .shipping-results{
                    margin-top: 10px;
                    display: table;
                    width: 100%;
                    border-collapse: collapse;
                }
                .shipping-results .result{
                    display: table-row;
                    border-bottom:  1px solid #ccc;
                    
                }
                .shipping-results .result > *{
                    display: table-cell;
                    padding: 5px 2px;
                }
                .shipping-results .result i{
                    font-family: "FontAwesome";
                }
                .myloader {
                  border: 16px solid #f3f3f3; /* Light grey */
                  border-top: 16px solid #bf3e1b; /* Blue */
                  border-radius: 50%;
                  width: 50px;
                  height: 50px;
                  animation: spin 2s linear infinite;
                }
                .shipping-form #cepvalue{
                        width: 120px;
                        text-align: center;
                }

                @keyframes spin {
                  0% { transform: rotate(0deg); }
                  100% { transform: rotate(360deg); }
                }
        </style>
        <div class="ajax-shipping" style="width:100%; display: inline-block">
                <p>Simule seu frete</p>
                <div class="shipping-form" style="display: flex;">   
                        <input type="hidden" name="idproduto" id="idproduto" value="<?php echo $id?>"> 
                        <input type="text" name="ajax-cep" id="cepvalue" placeholder="Digite seu cep" >
                        <button type="button" id="cepsend">Simular </button>
                </div>
                <div class="shipping-results">
                        <div class="wait"></div>
                </div>
        </div>
        <?php

}
